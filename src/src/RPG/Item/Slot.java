package src.RPG.Item;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
