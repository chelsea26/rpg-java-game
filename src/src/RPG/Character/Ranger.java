package src.RPG.Character;

import src.RPG.Exceptions.InvalidArmorException;
import src.RPG.Exceptions.InvalidWeaponException;
import src.RPG.Item.Armor;
import src.RPG.Item.Weapon;

public class Ranger extends CharacterAttribute {

    public Ranger(String name) {
        super(name, 1, 7, 1);
        setCharacterDPS("dexterity");
    }

    // level up + set attributes.
    public void levelup() {
        setLevel();
        updateBaseAttributes(1, 5, 1);
        //call set CharacterDPS to re-calculate
        setCharacterDPS("dexterity");
    }

    public void Equip(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.BOW) {
            setEquipment(weapon);
            //call set CharacterDPS to re-calculate
            setCharacterDPS("dexterity");
        } else {
            throw new InvalidWeaponException(super.getName() + " can't equip " + weapon.getWeaponType() + " but can equip: Axes, Hammers and Swords.");
        }
    }

    public void Equip(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.LEATHER || armor.getArmorType() == Armor.Type.MAIL) {
            setEquipment(armor);
            //call set CharacterDPS to re-calculate
            setCharacterDPS("dexterity");
        } else {
            throw new InvalidArmorException(super.getName() + " can't equip " + armor.getArmorType() + " but can equip: Mail, Plate.");
        }
    }
}
