
RPG
This is a role-playing game (RPG) that allows players to create and customize their own characters, level them up, and equip them with weapons and armor. The game features four character classes: Mage, Ranger, Rogue, and Warrior, each with their own unique attributes and abilities.

Requirements
Java 8 or higher
Getting Started
To run the RPG, you will need to have Java installed on your system. You can check if you have Java installed by running the following command in your terminal:

Copy code
java -version
If you don't have Java installed, you can download it from the Java website.

Once you have Java installed, you can run the RPG by navigating to the project directory and running the following command:

Copy code
java -jar rpg.jar
Character Classes
There are four character classes available in the RPG:

Mage: A magical class that excels in intelligence and dexterity. Can equip staves and wands as weapons, and cloth armor.
Ranger: A ranged class that excels in dexterity and strength. Can equip bows and crossbows as weapons, and leather and mail armor.
Rogue: A stealthy class that excels in dexterity and intelligence. Can equip daggers and swords as weapons, and leather and mail armor.
Warrior: A melee class that excels in strength and intelligence. Can equip axes, hammers, and swords as weapons, and mail and plate armor.
Each character class has its own set of starting attributes and abilities, and can level up to increase their attributes and unlock new abilities.

Equipment
Characters can equip weapons and armor to increase their attributes and damage. There are four slots for equipment:

Weapon: The character's main source of damage.
Head: Increases the character's intelligence.
Body: Increases the character's strength.
Legs: Increases the character's dexterity.
Testing
To run the test suite for the RPG, navigate to the project directory and run the following command:

Copy code
java -jar test.jar
This will run a series of tests to ensure that the character classes and equipment system are functioning correctly.

